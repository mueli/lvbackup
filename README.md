# LvBackup

This is a script which can be used to create backups of LVs. It creates full 
backups or incremental backups.

## Recommended Use

Clone the repository and copy (or link) the ```lvbackup``` script to a 
reasonable bin folder, e.g. ```/usr/local/bin```. For configuration of the 
script you have to copy the provided ```lvbackup.conf.example``` to 
```lvbackup.conf``` next to the executable and edit the parameters.

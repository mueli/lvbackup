#!/bin/bash
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 
# Author: Michael Hammer

script_dir=$(dirname "$0")

if [ ! -e "${script_dir}/lvbackup.conf" ]; then
    echo "We need a config file 'lvbackup.conf'"
    exit -1;
fi

source "${script_dir}/lvbackup.conf"

version="0.3-rc1"

TEMP=`getopt -o bfhsv -l backup,full,help,sim,version -n 'lvbackup' -- "$@"`
if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 254 ; fi
eval set -- "$TEMP"

full_backup=0  # global variable to detect if we create a full_backup or not
simulation=0   # global variable to set a simulated run

#------------------------------------------------------------------
function append_to_file() {
    if [ $simulation -eq 0 ]; then
	echo $1 >> $2
    fi
}

#------------------------------------------------------------------
function write_to_file() {
    if [ $simulation -eq 0 ]; then
	echo $1 > $2
    fi
}

#------------------------------------------------------------------
function read_actual_cycle() {
    day_number="`date +%w`"
    actual_cycle=1

    if [ ! -e "${db_dir}" ]; then
	mkdir -p "${db_dir}"
	write_to_file "1" "${actual_cycle_file}"
    else
	actual_cycle="`cat ${actual_cycle_file}`"
	if [ $day_number -eq 0 ]; then
	    full_backup=1
	    (( actual_cycle += 1 ))
	    if [ $actual_cycle -gt 4 ]; then
		actual_cycle=1
	    fi
	    write_to_file "$actual_cycle" "${actual_cycle_file}"
	fi
    fi
}

#------------------------------------------------------------------
function log() {
    log_decorator="(`date '+%a %H:%M:%S'`)"
    append_to_file "$log_decorator $1" "${backup_log}"
    echo "$log_decorator $1"
}

#------------------------------------------------------------------
function create_backup() {
    day_of_the_week="`date +%A | tr [:upper:] [:lower:]`"
    today="`date +%Y-%m-%d`"
    read_actual_cycle

    if [ -e ${backup_log} ]; then
	rm ${backup_log}
    fi

    log "LVBackup"
    log "---------------------------------"
    log "Backup Volumes : $backup_vols"

    if [ ! -e "${last_full}" ]; then
	full_backup=1
    fi

    if [ $full_backup -eq 1 ]; then
	append_to_file "${today} : ${backup_vols}" "${last_full}"
	append_to_file "FULL: ${today} : ${backup_vols}" "${last}"
	log "It's a FULL backup!"
    else
	append_to_file "INCR: ${today} : ${backup_vols}" "${last}"
	log "It's an INCREMENTAL backup!"
    fi
    
    # Main loop of all volumes to backup
    for backup_vol in $backup_vols; do
	lvcreate -L$backup_size -s -n "${backup_vol}_backup" "/dev/${volume_group}/${backup_vol}"
	if [ ! -d "/mnt/${backup_vol}_backup" ]; then
	    mkdir -p "/mnt/${backup_vol}_backup"
	fi
	mount "/dev/${volume_group}/${backup_vol}_backup" "/mnt/${backup_vol}_backup"
	
    # do the backup!!
	if [ $full_backup -eq 1 ]; then
	    log "FULL for ${backup_vol}"
	    if [ $simulation -eq 0 ]; then
		if [ -e "${db_dir}/${backup_vol}.snar" ]; then
		    rm "${db_dir}/${backup_vol}.snar"
		fi
		tar -cz -C "/mnt" --file="${backup_storage}/${backup_vol}_${today}.tgz" \
		    --no-check-device \
		    --listed-incremental="${db_dir}/${backup_vol}.snar" \
		    "${backup_vol}_backup"
#		find "/mnt/${backup_vol}_backup" -depth -print | cpio -o | gzip > "${backup_storage}/${backup_vol}_backup_${actual_cycle}_${today}.cpio.gz"
	    fi
	else
	    log "INCREMENTAL for ${backup_vol}"
	    if [ $simulation -eq 0 ]; then
		cp "${db_dir}/${backup_vol}.snar" "${db_dir}/${backup_vol}.snar-1"
		tar -cz -C "/mnt" --file="${backup_storage}/${backup_vol}_${day_of_the_week}${actual_cycle}.tgz" \
		    --no-check-device \
		    --listed-incremental="${db_dir}/${backup_vol}.snar-1" \
		    "${backup_vol}_backup"
#		find "/mnt/${backup_vol}_backup" -depth -newer "${last_full}" -print | cpio -o | gzip > "${backup_storage}/${backup_vol}_backup_${day_of_the_week}${actual_cycle}.cpio.gz"
	    fi
	fi

	umount "/mnt/${backup_vol}_backup"
	sleep 5
	lvremove -f "/dev/${volume_group}/${backup_vol}_backup"
    done

    log "Created gzip files: "
    if [ $simulation -eq 0 ]; then
	files=$(find "${backup_storage}" -newer "${last}" -a -name "*.tgz")
	iterable=$(echo $files | tr "\n" " ")
	for file in $iterable; do
	    log "$(ls -lh $file)"
	done
	log ""
    else
	log "NO files created - we are in SIMULATION mode!"
    fi
}

#------------------------------------------------------------------
function print_version() {
    echo "LVBackup version \"${version}\""
}

#------------------------------------------------------------------
function print_help () {
    echo "Script for Managing LVM backups" ; echo "";
    echo "Commands:"
    echo " -b --backup                 Create normal backup"
    echo " -f --full                   Create full backup"
    echo " -s --sim                    Only Simulate actions"
    echo ""
    echo " -h --help                   This help screen"
    echo " -v --version                Prints version string"
}

#------------------------------------------------------------------
while true ; do
    case "$1" in
	-s|--sim) simulation=1 ; shift ;;
	-b|--backup) full_backup=0 create_backup ; shift ;;
	-f|--full) full_backup=1 create_backup ; shift ;;
	-h|--help) print_help ; exit 0; shift ;;
	-v|--version) print_version ; exit 0; shift ;;
	--) shift ; break ;;
	*) break ; exit 1 ;;
    esac
done

exit 0;


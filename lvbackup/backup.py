from lvbackup import archive_db
from lvbackup.archive_db import ArchiveDB
from re import sub
from . import logger
from dynaconf import settings
from datetime import datetime
from pathlib import Path
import os
import time
import subprocess
import shutil
from typing import Dict, List
from . import helper
from .archive_db import ArchiveDB

class LVMBackup:
    def _create_snapshot(self, vg: str, lv: str) -> None:
        """Create LVM snapshot for the given volume group and logical volume

        Args:
            vg (str): volume group in which the logical volume to be backed up resides
            lv (str): logical volume the backup should be created for
        """
        logger.info(f"Create snapshot for {vg}/{lv}")
        helper.exec_cmd(["lvcreate", f"-L{settings.BACKUP_SIZE}", "-s", "-n", f"{lv}_backup", f"/dev/{vg}/{lv}"])
        self.ts = datetime.now()
        return

    def _mount_snapshot(self, vg: str, lv: str) -> None:
        """Mount the LVM snapshot

        Args:
            vg (str): volume group in which the logical volume to be backed up resides
            lv (str): logical volume the backup should be created for

        Raises:
            RuntimeError: Mount path exists but is not a directory
            RuntimeError: Mount path is already a mount point
        """
        mp = self._mnt_base.joinpath(vg, f"{lv}_backup")
        if not mp.exists():
            logger.debug("Create mount path {mp}")
            mp.mkdir(parents=True)
        else:
            if not mp.is_dir():
                raise RuntimeError(f"Mount path {mp} exists but is not a directory!")
            if mp.is_mount():
                raise RuntimeError(f"Mount path {mp} is already a mount point!")
        logger.info(f"Mounting snapshot {vg}/{lv}_backup to {mp}")
        helper.exec_cmd(["mount", f"/dev/{vg}/{lv}_backup", mp])
        return

    def _umount_snapshot(self, vg: str, lv: str) -> None:
        """Umount LVM snapshot

        Args:
            vg (str): volume group in which the logical volume to be backed up resides
            lv (str): logical volume the backup should be created for
        """
        mp = self._mnt_base.joinpath(vg, f"{lv}_backup")
        logger.info(f"Umounting snapshot {mp}")
        helper.exec_cmd(["umount", mp])
        return

    def _remove_snapshot(self, vg, lv):
        """Remove the LVM snapshot previously created

        Args:
            vg (str): volume group in which the logical volume to be backed up resides
            lv (str): logical volume the backup should be created for
        """
        logger.info(f"Remove snapshot for {vg}/{lv}")
        helper.exec_cmd(["lvremove", "-f", f"/dev/{vg}/{lv}_backup"])
        return

    def _create_archive(self, vg: str, lv: str, full_backup: bool = False) -> Dict:
        backup_storage = Path(settings.BACKUP_STORAGE)
        hostname = helper.getHostname()
        timestamp = helper.strftime( self.ts )
        backup_path = backup_storage.joinpath(hostname, vg)
        if not backup_path.exists():
            backup_path.mkdir(parents=True)
        else:
            if not backup_path.is_dir():
                raise RuntimeError(f"The path to store backup in '{backup_path}' exists but is not a directory!")
        backup_file = backup_path.joinpath(f"{lv}_{timestamp}.tgz")

        if full_backup:
            logger.info("We execute a full backup")
            snar_file = backup_path.joinpath(f"{lv}_{timestamp}.0.snar")
            backup_base = None
        else:
            logger.info("We execute an incremental backup")
            newest_full = self._archive_db.getNewestFullBackup(vg, lv)
            snar_file_full = backup_path.joinpath(f"{lv}_{helper.strftime(newest_full['timestamp'])}.0.snar")
            if not snar_file_full.exists():
                raise RuntimeError(f"We can not run incremental backup as the level 0 snar file for {vg}/{lv} ('{snar_file_full}') does not exist!")
            snar_file = backup_path.joinpath(f"{lv}_{timestamp}.1.snar")
            shutil.copy(snar_file_full, snar_file)
            backup_base = f"{lv}_{timestamp}"
            
        helper.exec_cmd(["tar", 
                        "--create", 
                        "--use-compress-program=pigz",
                        #"--gzip", 
                        "--no-check-device", 
                        f"--listed-incremental={snar_file}", 
                        f"--directory={backup_storage}", 
                        f"--file={backup_file}",
                        f"/mnt/lvbackup/{vg}/{lv}_backup"])
        if not settings.SIM:
            md5 = helper.md5sum( backup_file )
            file_size = backup_file.stat().st_size
        else:
            md5 = "foobar"
            file_size = 0
        
        return { "timestamp": self.ts , "vg": vg, "lv": lv, "file": backup_file.name , "file_size": file_size ,"md5": md5 ,"backup_base": backup_base }

    def create_backup(self):
        self._mnt_base = Path(settings.TEMP_MOUNT_BASEDIR)

        if settings.SIM:
            logger.warning(f"We run in 'Simulation' mode ... no backup files will be generated!")

        helper.mount_storage()

        self._archive_db = ArchiveDB()

        for lv_path in settings.LOGICAL_VOLUMES:
            logger.info(f"--> Creating backup of {lv_path}")
            vg = lv_path.split('/')[0]
            lv = lv_path.split('/')[1]

            self._create_snapshot(vg, lv)
            self._mount_snapshot(vg, lv)
            backup_info = self._create_archive(vg, lv, settings.FULL_BACKUP)
            self._archive_db.append(backup_info)
            logger.info(backup_info)
            self._umount_snapshot(vg, lv)
            if not settings.SIM:
                time.sleep(5)
            self._remove_snapshot(vg, lv)

            #TODO: Cleanup on Exception!
        
        if not settings.SIM:
            self._archive_db.save()

        #helper.umount_storage()


# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 
# Author: Michael Hammer

import argparse
import logging
from . import logger, version
from .backup import LVMBackup
import os

def run():
    parser = argparse.ArgumentParser(description='Backup tool for LVM data volumes')
    parser.add_argument('-f','--full_backup', action='store_true', help="If flag set a full backup is created - default is to create incremental backups")
    parser.add_argument('-s','--sim', action='store_true', help="If flag is set backup is only simluated - no backup files are created")
    parser.add_argument('-V','--version', action='version', version=version)
    parser.add_argument('--verbose', '-v', action='count', default=0, help="Define the verbosity level, '-vv' max verbosity which is supported")
    args = parser.parse_args()

    if args.verbose == 1:
        logger.setLevel(logging.INFO)
    if args.verbose > 1:
        logger.setLevel(logging.DEBUG)

    if args.sim:
        os.environ['DYNACONF_SIM']="true"
    if args.full_backup:
        os.environ['DYNACONF_FULL_BACKUP']="true"

    lvm_backup = LVMBackup()
    lvm_backup.create_backup()

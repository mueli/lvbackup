import os
import hashlib
from pathlib import Path
from dynaconf import settings
from . import logger
import subprocess
from typing import List

def getHostname():
    return os.uname()[1]

def md5sum( fp: Path ):
    m = hashlib.md5()
    with open(fp, "rb") as f:
        while chunk := f.read(8192):
            m.update(chunk)
    return m.hexdigest()

def strftime( ts ):
    return( ts.strftime("%Y-%m-%dT%H:%M:%S") )

def exec_cmd(cmd: List[str]) -> None:
    """Execute subprocess command only if we are not in "simulated" mode

    Args:
        cmd (List[str]): list of strings provided to subprocess.run()
    """
    if not settings.SIM:
        logger.debug(cmd)
        cp = subprocess.run(cmd)
        cp.check_returncode()
    else:
        logger.info(f"SIMULATED: {cmd}")
    return

def mount_storage():
    """Mounting the backup storage where the archives are going to be
    written to. This folder (configuration: BACKUP_STORAGE) needs to be in
    fstab (with 'noauto') so that mount can happen

    Raises: 
        RuntimeError: [description]
        RuntimeError: [description]
    """
    mp = Path(settings.BACKUP_STORAGE)
    if not mp.is_absolute():
        raise RuntimeError(f"Given storage path '{mp}' is not absolute!")
    if not (mp.exists() and mp.is_dir()):
        raise RuntimeError(f"Storage path is not a directory!")
    if mp.is_mount() and not settings.SIM:
        logger.info(f"Backup storage '{mp}' is already mounted!")
        return 
    logger.info(f"Mounting storage {mp}")
    exec_cmd(["mount", f"{mp}"])
    return

def umount_storage():
    """Umount the backup storage
    """
    mp = Path(settings.BACKUP_STORAGE)
    logger.info(f"Umounting storage {mp}")
    exec_cmd(["umount", f"{mp}"])
    return



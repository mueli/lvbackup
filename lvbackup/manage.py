from .archive_db import ArchiveDB
from .backup import LVMBackup

class Manage():
    def __init__(self):
        self._archive_db = ArchiveDB()

    def createBackup(self, vg:str, lv:str, full_backup:bool = False):
        """Creates a backup for a given logical volume

        Args:
            vg (str): [description]
            lv (str): [description]
            full_backup (bool, optional): [description]. Defaults to False.
        """

        

        return
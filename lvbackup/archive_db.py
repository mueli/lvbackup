from dynaconf import settings
from pathlib import Path

from pandas._libs.tslibs import timestamps
from . import helper
from . import logger
import pandas as pd

class ArchiveDB():
    def __init__(self):
        storage = Path(settings.BACKUP_STORAGE)
        helper.mount_storage()
        hostname = helper.getHostname()

        self.pickle_p = storage.joinpath(hostname,"archive.pickle")

        if not self.pickle_p.exists():
            logger.info("No archive db found - create new one")
            self.archive_db = pd.DataFrame(columns=[
                "timestamp",
                "vg",
                "lv",
                "file",
                "file_size",
                "backup_base"
            ])
        else:
            self.archive_db = pd.read_pickle(self.pickle_p)

    def save(self):
        self.archive_db.to_pickle(self.pickle_p)

    def getNewestFullBackup(self, vg, lv):
        t = self.archive_db.query("(vg == @vg) & (lv == @lv)")
        return( t.iloc[t['timestamp'].argmax()] )

    def append(self, backup_info):
        self.archive_db = self.archive_db.append( backup_info, ignore_index=True )
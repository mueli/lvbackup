import logging
from pathlib import Path

version = "0.4.1"

log_path = Path('/var/log/lvbackup')
if not log_path.exists():
    log_path.mkdir()
else:
    if not log_path.is_dir():
        raise RuntimeError("The log path '{log_path}' exists but is not a directory!")

logger = logging.getLogger('lvbackup')
ch = logging.StreamHandler()
fh = logging.FileHandler(log_path.joinpath('lvbackup.log'))
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)